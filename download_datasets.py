from kaggle.api.kaggle_api_extended import KaggleApi

api = KaggleApi()
api.authenticate()

to_download = {
    'sudalairajkumar/novel-corona-virus-2019-dataset' : 'datasets/novel-corona-virus-2019-dataset',
    'paultimothymooney/covid19-containment-and-mitigation-measures' : 'datasets/covid19-containment-and-mitigation-measures'
}

for ds, path in to_download.items():
    api.dataset_download_files(ds,path=path, force=False, quiet=True, unzip=True)
